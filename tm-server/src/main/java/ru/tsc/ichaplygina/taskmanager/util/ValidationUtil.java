package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class ValidationUtil {

    public static boolean isEmptyString(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

    public static boolean isInvalidListIndex(final long index, final long size) {
        return (index < 0 || index >= size);
    }

}
