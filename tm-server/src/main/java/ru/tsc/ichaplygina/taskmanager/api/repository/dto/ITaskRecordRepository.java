package ru.tsc.ichaplygina.taskmanager.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;

import java.util.List;

public interface ITaskRecordRepository extends IAbstractBusinessEntityRecordRepository<TaskDTO> {

    void clear();

    void clearForUser(@NotNull String userid);

    @NotNull
    List<TaskDTO> findAll();

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllByProjectIdForUser(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<TaskDTO> findAllForUser(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull String id);

    @Nullable
    TaskDTO findByIdForUser(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findByIndex(int index);

    @Nullable
    TaskDTO findByIndexForUser(@NotNull String userId, int index);

    @Nullable
    TaskDTO findByName(@NotNull String name);

    @Nullable
    TaskDTO findByNameForUser(@NotNull String userId, @NotNull String name);

    @Nullable
    TaskDTO findTaskInProject(@NotNull String taskId, @NotNull String projectId);

    @Nullable
    TaskDTO findTaskInProjectForUser(@NotNull String userId,
                                     @NotNull String taskId,
                                     @NotNull String projectId);

    @Nullable
    String getIdByIndex(int index);

    @Nullable
    String getIdByIndexForUser(@NotNull String userId, int index);

    @Nullable
    String getIdByName(@NotNull String name);

    @Nullable
    String getIdByNameForUser(@NotNull String userId, @NotNull String name);

    long getSize();

    long getSizeForUser(@NotNull String userId);

    void removeAllByProjectId(@NotNull String projectId);

    void removeById(@NotNull String id);

    void removeByIdForUser(@NotNull String userId, @NotNull String id);

    void removeByIndex(int index);

    void removeByIndexForUser(@NotNull String userId, int index);

    void removeByName(@NotNull String name);

    void removeByNameForUser(@NotNull String userId, @NotNull String name);

}
