package ru.tsc.ichaplygina.taskmanager.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.listener.EntityListener;

import javax.persistence.*;

import static java.lang.System.currentTimeMillis;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "tm_session")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractModelDTO implements Cloneable {

    @Nullable
    @Column
    private String signature;

    @Column(name = "time_stamp")
    private long timeStamp = currentTimeMillis();

    @NotNull
    @Column(name = "user_id")
    private String userId;

    public SessionDTO(@NotNull String userId) {
        this.userId = userId;
    }

    @Override
    public SessionDTO clone() throws CloneNotSupportedException {
        return (SessionDTO) super.clone();
    }

    @Override
    public String toString() {
        return getId() + " " + userId + "" + timeStamp;
    }
}
