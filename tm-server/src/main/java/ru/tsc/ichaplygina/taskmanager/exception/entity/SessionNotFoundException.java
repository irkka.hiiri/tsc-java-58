package ru.tsc.ichaplygina.taskmanager.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class SessionNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Session not found.";

    public SessionNotFoundException() {
        super(MESSAGE);
    }

}
