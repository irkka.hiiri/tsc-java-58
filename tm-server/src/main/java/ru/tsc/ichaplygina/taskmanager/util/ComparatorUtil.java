package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Sort;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public class ComparatorUtil {

    public static Comparator getComparator(@Nullable final String sortBy) {
        if (isEmptyString(sortBy)) return Sort.CREATED.getComparator();
        try {
            return Sort.valueOf(sortBy.toUpperCase()).getComparator();
        } catch (@NotNull final IllegalArgumentException e) {
            return Sort.CREATED.getComparator();
        }
    }

}
