package ru.tsc.ichaplygina.taskmanager.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IAbstractBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntity> extends AbstractService<E> implements IAbstractBusinessEntityService<E> {

    @NotNull
    @Autowired
    protected IUserService userService;

    @Override
    public abstract void add(@NotNull final String userId, @NotNull final String entityName, @Nullable final String entityDescription);

    @Override
    public abstract void clear(final String userId);

    @Nullable
    @Override
    public abstract E completeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E completeByName(@NotNull final String userId, @NotNull final String entityName);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId, @Nullable final String sortBy);

    @Nullable
    @Override
    public abstract E findById(@NotNull final String userId, @Nullable final String entityId);

    @Nullable
    @Override
    public abstract E findByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract String getId(@NotNull final String userId, @NotNull final String entityName);

    @Override
    public abstract long getSize(@NotNull final String userId);

    @Override
    public abstract boolean isEmpty(@NotNull final String userId);

    @Nullable
    @Override
    public abstract E removeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E removeByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E startById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E startByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E updateById(@NotNull final String userId,
                                 @NotNull final String entityId,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription);

}
