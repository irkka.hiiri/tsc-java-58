package ru.tsc.ichaplygina.taskmanager.configuration;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import ru.tsc.ichaplygina.taskmanager.api.property.IDatabaseProperty;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;
import ru.tsc.ichaplygina.taskmanager.dto.TaskDTO;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.model.Project;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.model.Task;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("ru.tsc.ichaplygina.taskmanager")
public class ServerConfiguration {

    @Bean
    @NotNull
    public EntityManagerFactory entityManagerFactory(@NotNull @Autowired IDatabaseProperty databaseProperty) {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(Environment.USER, databaseProperty.getDatabaseUsername());
        settings.put(Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(Environment.DIALECT, databaseProperty.getDatabaseSqlDialect());
        settings.put(Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, databaseProperty.getDatabaseShowSql());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseUseSecondLvlCache());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseCacheRegionPrefix());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseCacheFactoryClass());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseCacheConfigFile());
        @NotNull final StandardServiceRegistryBuilder registryBuilder =
                new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(@NotNull @Autowired final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
