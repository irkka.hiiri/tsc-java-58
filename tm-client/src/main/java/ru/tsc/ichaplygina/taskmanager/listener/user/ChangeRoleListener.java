package ru.tsc.ichaplygina.taskmanager.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

@Component
public final class ChangeRoleListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "change user's role";
    @NotNull
    public static final String NAME = "change role";

    @NotNull
    @Override
    public final String command() {
        return NAME;
    }

    @NotNull
    @Override
    public final String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@changeRoleListener.command() == #consoleEvent.name")
    public final void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final String role = readLine(ENTER_ROLE);
        getAdminEndpoint().changeRole(sessionService.getSession(), login, role);
    }

}
