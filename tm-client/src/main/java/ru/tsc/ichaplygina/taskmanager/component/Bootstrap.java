package ru.tsc.ichaplygina.taskmanager.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.event.ConsoleEvent;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.listener.AbstractListener;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
@Component
public final class Bootstrap {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    public void executeCommandByArgument(@NotNull final String argument) {
        @Nullable final AbstractListener listener = getListenerByArgument(argument);
        if (listener != null) publisher.publishEvent(new ConsoleEvent(listener.command()));
    }

    @Nullable
    private AbstractListener getListenerByArgument(@NotNull final String argument) {
        for (@NotNull final AbstractListener listener : listeners) {
            if (argument.equals(listener.argument())) return listener;
        }
        return null;
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    private String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void run(@NotNull final String... args) {
        initPID();
        fileScanner.init();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
